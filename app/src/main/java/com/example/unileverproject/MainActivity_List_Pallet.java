package com.example.unileverproject;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity_List_Pallet extends AppCompatActivity {
    public boolean onOptionsItemSelected(MenuItem item){

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent myIntent = new Intent(getApplicationContext(), MainActivity_Home.class);
        startActivityForResult(myIntent, 0);
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pallet);
    }
}
